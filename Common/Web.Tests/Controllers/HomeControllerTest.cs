﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Mvc.Controllers;
using EntityFrameworkORM;
using Data;

namespace Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        IDbContext fakeContext;
        IRepository<Entity> fakeRepository;

        [TestInitialize]
        public void Init()
        {
            fakeContext = new DataContext("test");      //because of wrong connection string it will cause error in tests. Need to mock context and repository further to fix
            fakeRepository = new DataRepository<Entity>(fakeContext);
        }

        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController(fakeRepository);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController(fakeRepository);

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController(fakeRepository);

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
