﻿using System;
using System.Collections.Generic;
using Castle.MicroKernel;
using Castle.Windsor;

namespace Common.Tests.Helpers
{
    /// <summary>
    /// https://github.com/castleproject/Windsor/blob/master/docs/handlers.md
    /// What is a handler
    ///Handlers are types implementing IHandler interface. Windsor uses handlers to resolve components for given services, and then to release them.Handlers also give access to ComponentModel which allows developers to programatically inspect components.
    /// </summary>
    public static class HandlersHelper
    {
        public static IReadOnlyCollection<IHandler> GetAllHandlers(this IWindsorContainer container)
        {
            return GetHandlersFor(typeof(object), container);
        }

        public static IReadOnlyCollection<IHandler> GetHandlersFor<T>(this T type, IWindsorContainer container) where T : Type
        {
            return container.Kernel.GetAssignableHandlers(type);
        }
    }
}
