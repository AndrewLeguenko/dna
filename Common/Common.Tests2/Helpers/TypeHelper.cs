﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Windsor;

namespace Common.Tests.Helpers
{
    public static class TypeHelper
    {
        public static IReadOnlyCollection<Type> GetImplementationTypesFor<T>(this T type, IWindsorContainer container) where T : Type
        {
            return type.GetHandlersFor(container)
                .Select(h => h.ComponentModel.Implementation)
                .OrderBy(t => t.Name)
                .ToList();
        }

        public static IReadOnlyCollection<Type> GetControllersFromApplicationAssembly(Predicate<Type> where)
        {
            return typeof(object).Assembly.GetExportedTypes()
                .Where(t => t.IsClass && t.IsAbstract == false)
                .Where(where.Invoke)
                .OrderBy(t => t.Name)
                .ToList();
        }
    }
}
