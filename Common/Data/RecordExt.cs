﻿using Data.Interfaces;
using System;

namespace Data
{
    public class RecordExt : IData
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
    }
}
