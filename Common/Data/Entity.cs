﻿using Data.Interfaces;
using System;

namespace Data
{
    public class Entity : IData
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
    }
}
