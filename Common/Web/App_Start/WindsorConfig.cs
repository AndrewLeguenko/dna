﻿using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Common.IoC;

namespace Web
{
    public class WindsorConfig
    {
        private static IWindsorContainer container;

        public static void Config()
        {
            container = new WindsorContainer()
                .Install(
                    //FromAssembly.This(),              //instantiate and install then all IWindsorInstallers from this assembly - now it's useless because all installers are contained in Common project
                    FromAssembly.Named("Common")           //instantiate and install then all IWindsorInstallers from Common assembly
                );
            var controllerFactory = new WindsorControllerFactory(container.Kernel);     //create WindsorControllerFactory and pass Windsor's kernel from container to it's constructor
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);          //and then attach the WindsorControllerFactory to MVC infrastructure instead of standart MVC controller factory
        }

        public static void Release()
        {
            container.Dispose();    //release container
        }
    }
}