﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Castle.Windsor;
using Mvc.Controllers;
using System.Reflection;

namespace Common.Tests.Helpers
{
    public static class TypeHelper
    {
        public static List<Type> GetImplementationTypesFor<T>(this T type, IWindsorContainer container) where T : Type
        {
            return type.GetHandlersFor(container)
                .Select(h => h.ComponentModel.Implementation)
                .OrderBy(t => t.Name)
                .ToList();
        }

        public static List<Type> GetTypesFromApplicationAssembly(Predicate<Type> where)
        {
            //return typeof(HomeController).Assembly.GetExportedTypes()       //define where HomeController is located and ONLY THEN loads the Assembly to that controller - Mvc.dll from this solution. 
                                                                            //That means it's impossible to use typeof(IController) - System.Web.Mvc.dll, or typeof(object) - mscorlib.dll
            return Assembly.Load("Mvc").GetExportedTypes()
                .Where(t => t.IsClass && t.IsAbstract == false)
                .Where(where.Invoke)
                .OrderBy(t => t.Name)
                .ToList();
        }
    }
}
