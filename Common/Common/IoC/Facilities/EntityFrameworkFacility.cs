﻿using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using EntityFrameworkORM;

namespace Common.IoC.Facilities
{
    public class EntityFrameworkFacility : AbstractFacility
    {
        protected string connectionString;

        protected override void Init()
        {
            Kernel.Register(
                Component.For<IDbContext>()
                    .ImplementedBy<DataContext>()
                    .DependsOn(Dependency.OnValue("connectionString", connectionString))    //set data context dependency - constructor parameter
                    //.DependsOn(Dependency.OnComponent<>
                    .LifestylePerWebRequest(),
                Component.For(typeof(IRepository<>))                                        //register generic repository
                    .ImplementedBy(typeof(DataRepository<>))
                    .LifestylePerWebRequest());
        }

        public void Config(string connectionString)
        {
            //var connectionString = FacilityConfig.Children["DNAConnection"];
            this.connectionString = connectionString;
        }
    }
}
