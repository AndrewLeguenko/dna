﻿using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
//using log4net.Config;

//[assembly: XmlConfigurator(Watch = true)]     //needed only in case of using log4net(not needed for NLog)
namespace Common.IoC.Installers
{    
    public class LoggerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //container.AddChildContainer(...);                             //just to investigate


            #region log4net configuration

            //XmlConfigurator.Configure();
            //container.AddFacility<LoggingFacility>(f => f.UseLog4Net());    //by default will look for log4net.config file in startup project(Web)
            ////container.AddFacility<LoggingFacility>(f => f.UseLog4Net("log4net.config"));  //doesn't work by some undefined reason

            #endregion


            #region NLog configuration

            container.AddFacility<LoggingFacility>(m => m.UseNLog("NLog.config"));
            //container.AddFacility<LoggingFacility>(m => m.UseNLog().WithConfig("NLog.config"));

            #endregion
        }
    }
}
