﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Data;
using Data.Interfaces;

namespace Common.IoC.Installers
{
    public class EntityInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            InstallEntities(container);
        }

        private void InstallEntities(IWindsorContainer container)
        {
            container.Register(
                Component.For<IData>().ImplementedBy<Entity>(),
                Component.For<IData>().ImplementedBy<Record>().Named("Record"),
                Component.For<IData>().ImplementedBy<RecordExt>().Named("RecordExt")
            );
        }
    }
}
