﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Common.IoC.Facilities;
using System.Configuration;

namespace Common.IoC.Installers
{
    public class ORMInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<EntityFrameworkFacility>(facility => facility.Config(
                ConfigurationManager.ConnectionStrings["DNAConnection"].ConnectionString));
        }
    }
}
