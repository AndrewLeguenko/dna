﻿using Data;
using EntityFrameworkORM;
using System;
using System.Web.Mvc;

namespace Mvc.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IRepository<Entity> repository;

        public HomeController(/*IDbContext context*/IRepository<Entity> repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            Log.Warn("HomeController: Index action processing");

            repository.Create(new Entity
            {
                Id = Guid.NewGuid(),
                Message = "Test message"
            });

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}